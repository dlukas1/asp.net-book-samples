﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers
{
    public class TestController //: Controller
    {
        public string Index(string id)
        {
            return $"Hello from testController/index id = {id}";
        }

        public string Hello(string id, int intId)
        {
            return $"hello from TC/Hello with id of {id} and intId of {intId}";
        }

        public string CustomINT(int id)
        {
            return $"Hello from testCOnt.custom INT with  id {id}";
        }

        public string Custom(string id)
        {
            return $"Hello from testCOnt.custom with anything id {id}";
        }
    }
}