﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using UrlAndRoutes.Infrastructure;

namespace UrlAndRoutes
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<RouteOptions>(options => {
            options.ConstraintMap.Add("weekday", typeof(WeekDayConstrains));
            options.LowercaseUrls = true;
            options.AppendTrailingSlash = true;
            
        });
            
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseStatusCodePages();
            app.UseDeveloperExceptionPage();
            app.UseStaticFiles();
            //app.UseMvcWithDefaultRoute();
            app.UseMvc(routes => {
                /*
                routes.Routes.Add(new LegacyRoute(
                    app.ApplicationServices,
                    "/articles/Windows.html",
                    "/old/.NET_1.0_Class_Library"));
                    */
                    /*
                routes.MapRoute(
                    name:"NewRoute",
                    template: "App/Do{action}",
                    defaults: new {controller="Home"}
                    );
                    */

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}"
                    );
                routes.MapRoute(
                    name:"out", 
                    template: "outbound/{controller=Home}/{action=Index}");
            });



            /*
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name:"MyRoute",
                    template:"{controller}/{action}/{id:weekday?}",
                    defaults: new {controller="Home", action="Index"}
                   // constraints: new {id = new WeekDayConstrains()}
                    );


                //псевдонимы для методов, которые перестали существовать
                routes.MapRoute(name: "ShopSchema2",
                    template: "Shop/OldAction",
                    defaults: new { controller = "Home", action = "Index" });

                //представим что был контроллер Shop, который потом заменили на Home:
                routes.MapRoute(
                    name:"ShopSchema",
                    template:"Shop/{action}",
                    defaults: new { controller = "Home"});


                //with mixed static and changing segments:
                //localhost:49674/XHome/Index
                routes.MapRoute(name:"", template:"X{controller}/action");

                routes.MapRoute(
                    name: "default", 
                    template: "{controller=Home}/{action=Index}" );
                //with static segment Public:
                routes.MapRoute(
                    name:"",
                    template:"Public/{controller=Home}/{action=Index}");
            });
            */
        }
    }
}
