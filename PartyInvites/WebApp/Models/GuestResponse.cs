﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class GuestResponse
    {
        [Required(ErrorMessage ="Please enter name")]
        public string Name { get; set; }
        [Required(ErrorMessage ="Please enter e-mali")]
        [RegularExpression(".+\\@.+\\..+", ErrorMessage ="Please enter valid e-mail")]
        public string Email { get; set; }
        [Required(ErrorMessage ="Please enter phone")]
        public string Phone { get; set; }
        [Required(ErrorMessage ="Please specify are you coming")]
        public bool? WillAttend { get; set; }
    }
}
