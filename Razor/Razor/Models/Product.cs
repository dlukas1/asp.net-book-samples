﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Razor.Models
{
    public class Product
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public string Category { get; set; }

        public Product(int id, string name, string desc, decimal price, string cat)
        {
            ProductId = id;
            Name = name;
            Description = desc;
            Price = price;
            Category = cat;
        }
        //empty constructor
        public Product() { }
    }
}
