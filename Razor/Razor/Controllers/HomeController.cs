﻿using Microsoft.AspNetCore.Mvc;
using Razor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Razor.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            Product[] array =
            {
                new Product{Name= "Kayak", Price = 275M},
                new Product{Name = "LifeJacket", Price = 48.85M},
                new Product{Name = "Soccer Ball", Price = 19.95M},
                new Product{Name = "Corner Flag", Price = 34.45M}
            };
            return View(array);
        }


    }
}
