﻿using LanguageFeatures.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LanguageFeatures.Controllers
{
    public class HomeController : Controller
    {
        

        /*
        public ViewResult Index()
        {
            List<string> results = new List<string>();

            foreach (Product p in Product.GetProducts())
            {
                string name = p?.Name ?? "<No Name>";
                decimal? price = p?.Price ?? 0;
                string relatedName = p?.Related?.Name ?? "<None>";
               // string inStock = (p.InStock) ? "in stock" : "out of stock";
                results.Add(string.Format($"Name: {name}, Price:{price:C2}, Related: {relatedName}"));
            }

            return View(results);
           
        }
        */
        public async Task<ViewResult> Index()
        {
            
            /*
            ShoppingCart cart = new ShoppingCart
            {
                Products = Product.GetProducts()
            };
            */
            var productArray = new[]
            {
                new Product{Name = "Kayak", Price = 275M},
                new Product{Name = "LifeJacket", Price = 48.95M},
                new Product{Name = "Soccer Ball", Price = 19.50M},
                new Product{Name = "Corner Flag", Price = 34.95M}
            };
            //decimal cartTotal = cart.TotalPrices();
            //decimal arrayTotal = productArray.TotalPrices();



            /*
            Func<Product, bool> nameFilter = delegate (Product prod)
            {
                return prod?.Name?[0] == 'K';
            };
            */
            decimal priceFilterTotal = productArray
                .Filter(p => (p?.Price ?? 0) >= 20)
                .TotalPrices();
            
            decimal nameFilterTotal = productArray
                .Filter(p => p?.Name?[0] == 'K')
                .TotalPrices();

            /*
            return View("Index", new string[]
            {
                $"Price Total: {priceFilterTotal:C2}",
                $"Name Total: {nameFilterTotal:C2}"
            });
            */
            /* return View(Product.GetProducts()
                 .Select(p => p?.Name));
                 */
            return View(productArray.Select(p =>
            $"{nameof(p.Name)}: {p.Name}, {nameof(p.Price)}: {p.Price}"
            ));
           /*
            long? length = await MyAsyncMethods.GetPageLength();
            return View(new string[] { $"Length: {length}" });
                */
        }
    }
}
