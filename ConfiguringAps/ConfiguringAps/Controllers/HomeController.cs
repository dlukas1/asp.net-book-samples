﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ConfiguringAps.Infrastructure;
using Microsoft.Extensions.Logging;

namespace ConfiguringAps.Controllers
{
    public class HomeController : Controller
    {
        private UptimeService uptime;
        private ILogger<HomeController> logger;

        public HomeController(UptimeService up, ILogger<HomeController> log)
        {
            uptime = up;
            logger = log;
        }

        public ViewResult Index(bool throwException = false)
        {
            if (throwException)
            {
                throw new System.NullReferenceException();
            }
            logger.LogDebug($"Handeled {Request.Path} at uptime {uptime.Uptime}");
            return View(new Dictionary<string, string>
            {
                ["Message"] = "This is the Index ation",
                ["Uptime"] = $"{uptime.Uptime} ms"
            });
        }

        public ViewResult Error()
        {
            return View("Index", new Dictionary<string, string>
            {
                ["Message"] = "This is error action"
            });
        }
            
    }
}
