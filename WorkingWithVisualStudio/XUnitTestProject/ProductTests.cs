using System;
using WorkingWithVisualStudio.Models;
using Xunit;

namespace XUnitTestProject
{
    public class UnitTest1
    {
        [Fact]
        public void CanChangeName()
        {
            //Arrange
            Product p = new Product { Name = "Test", Price = 100M };
            //Act
            p.Name = "New Name";
            //Assert
            Assert.Equal("New Name", p.Name);
        }

        [Fact]
        public void CanChangePrice()
        {
            //Arrange
            Product p = new Product { Name = "Test", Price = 100M };
            //Act
            p.Price = 200M;
            //Assert
            Assert.Equal(200M, p.Price);
        }
    }
}
