﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using WorkingWithVisualStudio.Models;

namespace WorkingWithVisualStudio.Tests
{
    public class ProductTestData : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[] { GetPriceUnder50() };
            yield return new object[] { GetPriceOver50 };
        }

        private IEnumerable<Product> GetPriceUnder50()
        {
            decimal[] prices = new decimal[] {275, 49.95M, 19.55M, 24.55M};
            for (int i = 0; i < prices.Length; i++)
            {
                yield return new Product { Name = $"P{i + 1}", Price = prices[i] };
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        private Product[] GetPriceOver50 => new Product[]
        {
            new Product{Name = "P1", Price = 5},
            new Product{Name = "P2", Price = 15},
            new Product{Name = "P3", Price = 22.75M},
            new Product{Name = "P4", Price = 57.35M}
        };
    }
}
